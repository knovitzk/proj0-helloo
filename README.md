# Author
-------------

Kyra Novitzky

## Contact Address
---------------

knovitzk@uoregon.edu

## Description
---------------

This software uses a configuration file and configuration constant
in order to print the phrase "Hello world".
